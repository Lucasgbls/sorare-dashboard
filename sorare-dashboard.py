# SORARE-DASHBOARD - USEFULL STATS FOR SORARE MANAGERS

#------------------------------------------------------------------------

import json
import urllib.request
import requests
from re import sub
from bs4 import BeautifulSoup
from datetime import date, datetime
from PIL import Image
from io import BytesIO
from urllib.request import Request, urlopen
from dateutil.parser import parse
from datetime import timedelta


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    RED = '\033[1;31m'
    YELLOW = '\033[33m'


#links of the soraredata api
urlAverageValue = Request('https://www.soraredata.com/api/manager/value/0xc2b718f36aef9cb18a62b461ee295b2208949dd3', headers={'User-Agent': 'Mozilla/5.0'})
urlHome = Request('https://www.soraredata.com/api/stats/homeDashboard', headers={'User-Agent': 'Mozilla/5.0'})
urlManagerInfo = Request('https://www.soraredata.com/api/manager/info/temujin', headers={'User-Agent': 'Mozilla/5.0'})
urlPersonalStats = Request('https://www.soraredata.com/api/manager/stats/temujin', headers={'User-Agent': 'Mozilla/5.0'})

def parseJson():

    # the data we want to collect
    sorareAdress = ""
    averageValue = 0

    auctionVolume = 0
    auctionVariation = 0
    offerVolume = 0
    offerVariation = 0
    sdIndex = 0
    sdVariation = 0

    loomAdress = ""
    clubName = ""
    creationDate = ""
    currentEthBalance = ""
    numberOfRareCards = 0
    numberOfSRCards = 0
    numberOfUniqueCards = 0
    clubLogoUrl = ""

    totalBids = 0
    auctionWon = 0
    ethSpentAuctions = ""
    ethSpentSecondary = ""
    ethEarnedSecondary = ""
    listingsRate = 0

    with urllib.request.urlopen(urlHome) as url:
        data = json.loads(url.read().decode())

        auctionVolume = data['auction_volume_today']
        auctionVariation = data['variation_yesterday_auction']
        offerVariation = data['variation_yesterday_offer']
        offerVolume = data['offer_volume_today']
        sdIndex = data['sd_index']['index']
        sdVariation = data['sd_index']['variation']

    print(' ' + date.today().strftime("%d/%m/%Y") + '\n')
    if(auctionVariation < 0):
        print(bcolors.OKCYAN + ' auction volume 24h : ' + bcolors.ENDC + str(round(auctionVolume, 2)) + 'Ξ' + ' | ' + bcolors.RED + str(round(auctionVariation, 2)) + bcolors.ENDC)
    if(auctionVariation >= 0):
        print(bcolors.OKCYAN + ' auction volume 24h : ' + bcolors.ENDC + str(round(auctionVolume, 2)) + 'Ξ' + ' | ' + bcolors.OKGREEN + str(round(auctionVariation, 2)) + bcolors.ENDC)
    if(offerVariation < 0):
        print(bcolors.OKCYAN + ' offer volume 24h : ' + bcolors.ENDC + str(round(offerVolume, 2)) + 'Ξ | ' + bcolors.RED + str(round(offerVariation, 2)) + bcolors.ENDC)
    if (offerVariation >= 0):
        print(bcolors.OKCYAN + ' offer volume 24h : ' + bcolors.ENDC + str(round(offerVolume, 2)) + 'Ξ | ' + bcolors.OKGREEN + str(round(offerVariation, 2)) + bcolors.ENDC)
    if(sdVariation < 0):
        print(bcolors.OKCYAN + ' sd50 index : ' + bcolors.ENDC + str(round(sdIndex, 2)) + 'Ξ | ' + bcolors.RED + str(round(sdVariation, 2)) + bcolors.ENDC + '\n\n')
    if(sdVariation >= 0):
        print(bcolors.OKCYAN + ' sd50 index : ' + bcolors.ENDC + str(round(sdIndex, 2)) + 'Ξ | ' + bcolors.OKGREEN + str(round(sdVariation, 2)) + bcolors.ENDC + '\n\n')


    with urllib.request.urlopen(urlAverageValue) as url:
        data = json.loads(url.read().decode())

        sorareAdress = data['Address']
        averageValue = data['TotalValue']


    with urllib.request.urlopen(urlManagerInfo) as url:
        data = json.loads(url.read().decode())

        loomAdress = data['manager']['LoomAddress']
        clubName = data['manager']['TeamName']
        creationDate = data['manager']['CreationDate']
        currentEthBalance = data['balance']
        numberOfRareCards = data['cards_count']['rare_cards']
        numberOfSRCards = data['cards_count']['super_rare_cards']
        numberOfUniqueCards = data['cards_count']['unique_cards']
        clubLogoUrl = data['manager']['PictureUrl']

        print(bcolors.OKCYAN + ' loom adress : ' + bcolors.ENDC + loomAdress)
        print(bcolors.OKCYAN + ' sorare adress : ' + bcolors.ENDC + sorareAdress + '\n')
        print(bcolors.OKCYAN + ' club creation date : ' + bcolors.ENDC + creationDate)
        print(bcolors.OKCYAN + ' club name : ' + bcolors.ENDC + clubName)
        #print(bcolors.OKCYAN + ' current eth balance : ' + bcolors.ENDC  + bcolors.UNDERLINE + str(round(float(currentEthBalance), 3)) + bcolors.ENDC + 'Ξ\n')

        print(bcolors.OKCYAN + ' number of rare players : ' + bcolors.ENDC + str(numberOfRareCards))
        print(bcolors.OKCYAN + ' number of super-rare players : ' + bcolors.ENDC + str(numberOfSRCards))
        print(bcolors.OKCYAN + ' number of unique players : ' + bcolors.ENDC + str(numberOfUniqueCards) + '\n\n')


    print(bcolors.OKCYAN + ' roster 1-month avg value : ' + bcolors.ENDC + bcolors.UNDERLINE + str(round(averageValue, 2)) + bcolors.ENDC + 'Ξ\n')


    with urllib.request.urlopen(urlPersonalStats) as url:
        data = json.loads(url.read().decode())

        totalBids = data['total_auctions']
        auctionWon = data['auctions_won']
        ethSpentAuctions = data['auctions_total_spent']
        ethSpentSecondary = data['offers_spent_amount']
        ethEarnedSecondary = data['offers_received_amount']
        listingsRate = data['listing_stats']['percentage']

        maxiValue = 0
        mEth = 0
        cardsAmount = data['cards_amount']
        for cur in cardsAmount:
            if cur['value_usd'] > maxiValue:
                maxiValue = cur['value_usd']
                maxiDate = cur['date']
                maxiEth = cur['value']
            if cur['value'] > mEth:
                mEth = cur['value']
                mDate = cur['date']
                mValue = cur['value_usd']

        cardsStats1Usd = round(data['cards_amount'][-1]['value_usd'], 2)
        cardsStats0Usd = round(data['cards_amount'][-2]['value_usd'], 2)
        cardsStats2Usd = round(data['cards_amount'][-7]['value_usd'], 2)
        cardsStats3Usd = round(data['cards_amount'][-30]['value_usd'], 2)
        cardsStats4Usd = round(data['cards_amount'][-365]['value_usd'], 2)
        cardsStats5Usd = round(data['cards_amount'][-182]['value_usd'], 2)
        cardsStats1 = round(data['cards_amount'][-1]['value'], 2)
        cardsStats0 = round(data['cards_amount'][-2]['value'], 2)
        cardsStats2 = round(data['cards_amount'][-7]['value'], 2)
        cardsStats3 = round(data['cards_amount'][-30]['value'], 2)
        cardsStats4 = round(data['cards_amount'][-365]['value'], 2)
        cardsStats5 = round(data['cards_amount'][-182]['value'], 2)
        variationUsd2 = round(cardsStats1Usd - cardsStats2Usd, 2)
        variationUsd0 = round(cardsStats1Usd - cardsStats0Usd, 2)
        variationUsd3 = round(cardsStats1Usd - cardsStats3Usd, 2)
        variationUsd4 = round(cardsStats1Usd - cardsStats4Usd, 2)
        variationUsd5 = round(cardsStats1Usd - cardsStats5Usd, 2)
        variation2 = round(cardsStats1 - cardsStats2, 2)
        variation0 = round(cardsStats1 - cardsStats0, 2)
        variation3 = round(cardsStats1 - cardsStats3, 2)
        variation4 = round(cardsStats1 - cardsStats4, 2)
        variation5 = round(cardsStats1 - cardsStats5, 2)

    print('--------------------------------------------------------------------------------')
    print(bcolors.OKCYAN + ' 1-month avg roster value : ' + bcolors.ENDC + bcolors.UNDERLINE + str(cardsStats1) + bcolors.ENDC + 'Ξ')
    if(variation0 < 0):
        print(bcolors.OKCYAN + ' yesterday was : ' + bcolors.ENDC + str(cardsStats0) + 'Ξ | ' + bcolors.RED + str(variation0) + 'Ξ' + bcolors.ENDC)
    if(variation0 >= 0):
        print(bcolors.OKCYAN + ' yesterday was : ' + bcolors.ENDC + str(cardsStats0) + 'Ξ | ' + bcolors.OKGREEN + str(variation0) + 'Ξ' + bcolors.ENDC)
    if(variation2 < 0):
        print(bcolors.OKCYAN + ' one week ago was : ' + bcolors.ENDC + str(cardsStats2) + 'Ξ | ' + bcolors.RED + str(variation2) + 'Ξ' + bcolors.ENDC)
    if(variation2 >= 0):
        print(bcolors.OKCYAN + ' one week ago was : ' + bcolors.ENDC + str(cardsStats2) + 'Ξ | ' + bcolors.OKGREEN + str(variation2) + 'Ξ' + bcolors.ENDC)
    if(variation3 < 0):
        print(bcolors.OKCYAN + ' one month ago was : ' + bcolors.ENDC + str(cardsStats3) + 'Ξ | ' + bcolors.RED + str(variation3) + 'Ξ' + bcolors.ENDC)
    if(variation3 >= 0):
        print(bcolors.OKCYAN + ' one month ago was : ' + bcolors.ENDC + str(cardsStats3) + 'Ξ | ' + bcolors.OKGREEN + str(variation3) + 'Ξ' + bcolors.ENDC)

    if(variation5 < 0):
        print(bcolors.OKCYAN + ' six months ago was : ' + bcolors.ENDC + str(cardsStats5) + 'Ξ | ' + bcolors.RED + str(variation5) + 'Ξ' + bcolors.ENDC)
    if(variation5 >= 0):
        print(bcolors.OKCYAN + ' six months ago was : ' + bcolors.ENDC + str(cardsStats5) + 'Ξ | ' + bcolors.OKGREEN + str(variation5) + 'Ξ' + bcolors.ENDC)

    if(variation4 < 0):
        print(bcolors.OKCYAN + ' one year ago was : ' + bcolors.ENDC + str(cardsStats4) + 'Ξ | ' + bcolors.RED + str(variation4) + 'Ξ' + bcolors.ENDC)
    if(variation4 >= 0):
        print(bcolors.OKCYAN + ' one year ago was : ' + bcolors.ENDC + str(cardsStats4) + 'Ξ | ' + bcolors.OKGREEN + str(variation4) + 'Ξ' + bcolors.ENDC + '\n')


    print(bcolors.OKCYAN + ' 1-month avg roster value : ' + bcolors.ENDC + bcolors.UNDERLINE + str(cardsStats1Usd) + bcolors.ENDC + '$')
    if(variationUsd0 < 0):
        print(bcolors.OKCYAN + ' yesterday was : ' + bcolors.ENDC + str(cardsStats0Usd) + '$ | ' + bcolors.RED + str(variationUsd0) + '$' + bcolors.ENDC)
    if(variationUsd0 >= 0):
        print(bcolors.OKCYAN + ' yesterday was : ' + bcolors.ENDC + str(cardsStats0Usd) + '$ | ' + bcolors.OKGREEN + str(variationUsd0) + '$' + bcolors.ENDC)
    if(variationUsd2 < 0):
        print(bcolors.OKCYAN + ' one week ago was : ' + bcolors.ENDC + str(cardsStats2Usd) + '$ | ' + bcolors.RED + str(variationUsd2) + '$' + bcolors.ENDC)
    if(variationUsd2 >= 0):
        print(bcolors.OKCYAN + ' one week ago was : ' + bcolors.ENDC + str(cardsStats2Usd) + '$ | ' + bcolors.OKGREEN + str(variationUsd2) + '$' + bcolors.ENDC)
    if(variationUsd3 < 0):
        print(bcolors.OKCYAN + ' one month ago was : ' + bcolors.ENDC + str(cardsStats3Usd) + '$ | ' + bcolors.RED + str(variationUsd3) + '$' + bcolors.ENDC)
    if(variationUsd3 >= 0):
        print(bcolors.OKCYAN + ' one month ago was : ' + bcolors.ENDC + str(cardsStats3Usd) + '$ | ' + bcolors.OKGREEN + str(variationUsd3) + '$' + bcolors.ENDC)

    if(variationUsd5 < 0):
        print(bcolors.OKCYAN + ' six months ago was : ' + bcolors.ENDC + str(cardsStats5Usd) + '$ | ' + bcolors.RED + str(variationUsd5) + '$' + bcolors.ENDC)
    if(variationUsd5 >= 0):
        print(bcolors.OKCYAN + ' six months ago was : ' + bcolors.ENDC + str(cardsStats5Usd) + '$ | ' + bcolors.OKGREEN + str(variationUsd5) + '$' + bcolors.ENDC)

    if(variationUsd4 < 0):
        print(bcolors.OKCYAN + ' one year ago was : ' + bcolors.ENDC + str(cardsStats4Usd) + '$ | ' + bcolors.RED + str(variationUsd4) + '$' + bcolors.ENDC)
    if(variationUsd4 >= 0):
        print(bcolors.OKCYAN + ' one year ago was : ' + bcolors.ENDC + str(cardsStats4Usd) + '$ | ' + bcolors.OKGREEN + str(variationUsd4) + '$' + bcolors.ENDC)
    print('--------------------------------------------------------------------------------\n')

    print(bcolors.OKCYAN + ' all time highest dollars value : ' + bcolors.ENDC + str(maxiEth) + 'Ξ (' + str(maxiValue) + '$) on date : ' + maxiDate)
    print(bcolors.OKCYAN + ' all time highest ethereum value : ' + bcolors.ENDC + str(mEth) + 'Ξ (' + str(mValue) + '$) on date : ' + mDate + '\n\n')

    print(bcolors.OKCYAN + ' total auctions : ' + bcolors.ENDC + str(totalBids))
    print(bcolors.OKCYAN + ' auctions won : ' + bcolors.ENDC + str(auctionWon))
    print(bcolors.OKCYAN + ' eth spent on auctions : ' + bcolors.ENDC + ethSpentAuctions + 'Ξ\n')

    print(bcolors.OKCYAN + ' eth spent on the secondary market : ' + bcolors.ENDC + ethSpentSecondary + 'Ξ')
    print(bcolors.OKCYAN + ' eth earned on the secondary market : ' + bcolors.ENDC + ethEarnedSecondary + 'Ξ')
    print(bcolors.OKCYAN + ' rate of public listings sold : ' + bcolors.ENDC + str(round(listingsRate, 2)) + '%\n')
    print('--------------------------------------------------------------------------------\n\n')

    print(' ' + bcolors.OKCYAN + 'availability of players in u23 divisions' + bcolors.ENDC + '\n\n')


    now = datetime.now().isoformat()
    nowYear = now[0 : 4]
    nowMonth = now[5 : 7]
    nowDay = now[8 : 10]

    notUnder23 = [] # Red
    lastSeason = [] # Warning
    twoSeasonOrMore = [] # Green

    with open('birthdates.json') as json_file:
        data = json.load(json_file)
        val = data['data']['user']['cards']
        for c in val:
            if c['rarity'] != 'common':
                playerName = c['name']
                playerAge = c['player']['age']
                playerBirthdate = c['player']['birthDate']
                bdayYear = playerBirthdate[0 : 4]
                bdayMonth = playerBirthdate[5 : 7]
                bdayDay = playerBirthdate[8 : 10]

                string = playerName + '   born on   ' + bdayDay + ' : ' + bdayMonth + ' : ' + bdayYear

                if int(nowYear) - int(bdayYear) < 23 : # good for 2 years or more
                    twoSeasonOrMore.append(string)
            
                elif int(nowYear) - int(bdayYear) == 23 : # good for the season + 1 more if born after 1st june
                    if int(bdayMonth) < 7:
                        lastSeason.append(string)
                    else :
                        twoSeasonOrMore.append(string)
                elif int(nowYear) - int(bdayYear) == 24 : # good for the season only if born before 1st june
                    if int(bdayMonth) < 7:
                        notUnder23.append(string)
                    else :
                        lastSeason.append(string)
                elif int(nowYear) - int(bdayYear) > 24 : # not available for u23
                    notUnder23.append(string)

        for elm in notUnder23:
            print(' ' + bcolors.RED + elm + bcolors.ENDC)

        print('\n')

        print(bcolors.OKCYAN + ' players available until july 1st :' + bcolors.ENDC)

        print('\n')
            
        for elm in lastSeason:
            print(' ' + bcolors.WARNING + elm + bcolors.ENDC)

        print('\n')

        print(bcolors.OKCYAN + ' players available for the current and the next season at least :' + bcolors.ENDC)

        print('\n')

        for elm in twoSeasonOrMore:
            print(' ' + bcolors.OKGREEN + elm + bcolors.ENDC)

parseJson()

#------------------------------------------------------------------------
